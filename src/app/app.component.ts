import { Component } from '@angular/core';
import { TeamManagerService } from './services/team-manager/team-manager.service';
import { MarvelApiService } from './services/marvel-api/marvel-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'marvel-project';
}
