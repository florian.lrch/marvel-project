import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { CartComponent } from './cart/cart.component';
import { HerosListComponent } from './heros-list/heros-list.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { TeamsComponent } from './teams/teams.component';
import { CartDetailComponent } from './cart-detail/cart-detail.component';


@NgModule({
  declarations: [
    AppComponent,
    CartComponent,
    HerosListComponent,
    HeroDetailComponent,
    TeamsComponent,
    CartDetailComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
