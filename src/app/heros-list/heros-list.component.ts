import { Component, OnInit } from '@angular/core';
import {MarvelApiService} from '../services/marvel-api/marvel-api.service';
import {Hero} from '../models/hero';

@Component({
  selector: 'app-heros-list',
  templateUrl: './heros-list.component.html',
  styleUrls: ['./heros-list.component.css']
})
export class HerosListComponent implements OnInit {

  /**
   * Contient la liste des héros à afficher après une recherche
   */
  public heroesSearch: Array<Hero> = [];

  constructor(
    public marvelApi: MarvelApiService
  ) { }

  ngOnInit(): void {
    this.heroesSearch = this.marvelApi.getHerosArray();
  }

  public getTenRandomHeroes(): void {
    this.heroesSearch = [];
    this.marvelApi.getTenRandomHeros();
    this.heroesSearch = this.marvelApi.getHerosArray();
  }

  /**
   * Recherche disponible depuis le bouton ou en pressant "Entrer"
   * Les recherches de moins de deux caractères ne sont pas autorisées
   */
  public searchHeros(str, event): void {
    if ((event.key === 'Enter' || event.type === 'click') && str.length > 1) {
      this.heroesSearch = [];
      this.marvelApi.searchHeros(str);
      this.heroesSearch = this.marvelApi.getHerosArray();
    }
  }

  public isInit(): boolean {
    return this.marvelApi.getInit();
  }

}
