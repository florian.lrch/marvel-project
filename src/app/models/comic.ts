export interface Comic {
  id: number;
  title: string;
  description: string;
  dates: {
    type: string;
    date: string;
  };
  thumbnail: {
    path: string;
    extension: string;
  };
}
